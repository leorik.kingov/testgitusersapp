package ru.leorikwhite.testgitusersapp.api

import io.reactivex.rxjava3.core.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import ru.leorikwhite.testgitusersapp.data.User
import ru.leorikwhite.testgitusersapp.data.UserDetails

interface GitService {

    @GET("users")
    fun getUsers(
        @Query("since") sinceId:Int
    ): Observable<List<User>>

    @GET("users/{userName}")
    fun getUserDetailsById(
        @Path("userName") userName:String
    ): Observable<UserDetails>
}