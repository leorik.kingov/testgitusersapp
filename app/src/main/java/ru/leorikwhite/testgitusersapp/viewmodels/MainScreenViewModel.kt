package ru.leorikwhite.testgitusersapp.viewmodels

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observer
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.coroutines.launch
import ru.leorikwhite.testgitusersapp.data.User
import ru.leorikwhite.testgitusersapp.data.UserRepository
import javax.inject.Inject

@HiltViewModel
class MainScreenViewModel @Inject constructor(
    private val userRepository: UserRepository
) : ViewModel()  {

    private var _userList = MutableLiveData<List<User>>()
    val userList
        get() = _userList

    private var _isLoading = false
    val isLoading
        get() = _isLoading

    private var _isLastPage = false
    val isLastPage
        get() = _isLastPage

    lateinit var disposable: Disposable

    init {
        getUsers(0)
    }

    fun getUsers(since:Int) {
        val response = userRepository.getUsers(since)
        response.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(getUserListObserver())
    }

    private fun getUserListObserver():Observer<List<User>> {
        return object : Observer<List<User>> {
            override fun onSubscribe(d: Disposable) {
                disposable = d
            }

            override fun onError(e: Throwable) {
                Log.e("User error",e.message.toString())
            }

            override fun onComplete() {
                _isLoading = false
            }

            override fun onNext(t: List<User>) {
                _isLoading = true
                _userList.postValue(t)
                if(!_userList.value.isNullOrEmpty()) if(_userList.value?.size!! < 30) _isLastPage = true
            }

        }
    }


}