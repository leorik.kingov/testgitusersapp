package ru.leorikwhite.testgitusersapp.viewmodels

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observer
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers
import ru.leorikwhite.testgitusersapp.data.User
import ru.leorikwhite.testgitusersapp.data.UserDetails
import ru.leorikwhite.testgitusersapp.data.UserRepository
import javax.inject.Inject

class UserDetailsViewModel @AssistedInject constructor(
    private val userRepository: UserRepository,
    @Assisted private val userName:String
) : ViewModel() {

    @AssistedFactory
    interface UserDetailsViewModelFactory {
        fun create(userName: String): UserDetailsViewModel
    }

    companion object {

        fun providesFactory(
            assistedFactory: UserDetailsViewModelFactory,
            userName: String
        ): ViewModelProvider.Factory = object : ViewModelProvider.Factory {
            override fun <T : ViewModel> create(modelClass: Class<T>): T {
                return assistedFactory.create(userName) as T
            }
        }

    }

    private val _userDetails = MutableLiveData<UserDetails>()
    val userDetails
        get() = _userDetails

    private var _isLoading = false
    val isLoading
        get() = _isLoading

    lateinit var disposable: Disposable

    init{
        getUserDetails(userName)
    }

    fun getUserDetails(userName:String) {
        val response = userRepository.getUserDetailsById(userName)
        response.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(getUserDetailsObserver())
    }

    private fun getUserDetailsObserver(): Observer<UserDetails> {
        return object : Observer<UserDetails> {
            override fun onSubscribe(d: Disposable) {
                disposable = d
            }

            override fun onError(e: Throwable) {
                Log.e("User details error",e.message.toString())
            }

            override fun onComplete() {
                _isLoading = false
            }

            override fun onNext(t: UserDetails) {
                _isLoading = true
                _userDetails.postValue(t)
            }

        }
    }

}