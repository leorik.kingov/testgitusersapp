package ru.leorikwhite.testgitusersapp

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class TestGitUsersApp : Application() {
}