package ru.leorikwhite.testgitusersapp.view

import android.app.Application
import android.content.Intent
import android.os.Bundle
import androidx.activity.enableEdgeToEdge
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import dagger.hilt.android.AndroidEntryPoint
import ru.leorikwhite.testgitusersapp.R
import ru.leorikwhite.testgitusersapp.data.User
import ru.leorikwhite.testgitusersapp.di.ImageModule
import ru.leorikwhite.testgitusersapp.viewmodels.MainScreenViewModel

@AndroidEntryPoint
class MainScreenActivity : AppCompatActivity() {

    private val viewModel:MainScreenViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_main_screen)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }

        val recyclerView:RecyclerView = findViewById(R.id.userRecycler)
        val layoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = layoutManager
        val userListObserver = Observer<List<User>> {
            recyclerView.adapter = UserRecyclerAdapter(it, ImageModule(this.application),this.application) { user -> adapterItemClick(user) }
        }

        //Pagination
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val visibleItemCount = layoutManager.childCount
                val totalItemCount = layoutManager.itemCount
                val firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition()
                if (visibleItemCount + firstVisibleItemPosition >= totalItemCount && !viewModel.isLoading && !viewModel.isLastPage) {
                    viewModel.getUsers(viewModel.userList.value?.last()?.id ?: 0)
                }
            }
        })

        val userListPullToRefresh = findViewById<SwipeRefreshLayout>(R.id.user_swipe_to_refresh)

        userListPullToRefresh.setOnRefreshListener {
            viewModel.getUsers(0)
            userListPullToRefresh.isRefreshing = false
        }

        viewModel.userList.observe(this,userListObserver)



    }

    private fun adapterItemClick(user: User) {
        val intent = Intent(this, UserDetailsActivity::class.java)
        intent.putExtra("userName",user.login.toString())
        startActivity(intent)
    }
}