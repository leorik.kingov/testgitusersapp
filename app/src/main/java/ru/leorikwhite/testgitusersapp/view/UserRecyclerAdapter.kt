package ru.leorikwhite.testgitusersapp.view

import android.app.Application
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import coil.request.ImageRequest
import ru.leorikwhite.testgitusersapp.R
import ru.leorikwhite.testgitusersapp.data.User
import ru.leorikwhite.testgitusersapp.di.ImageModule
import ru.leorikwhite.testgitusersapp.viewmodels.MainScreenViewModel

class UserRecyclerAdapter(
    private val users: List<User>,
    private val imageModule:ImageModule,
    private val context:Application,
    private val userAdapterCallback: (User) -> Unit
) : RecyclerView
.Adapter<UserRecyclerAdapter.UserViewHolder>()
{
    class UserViewHolder(itemView:View) : RecyclerView.ViewHolder(itemView) {
        var userName:TextView = itemView.findViewById(R.id.userNameTextView)
        var userId:TextView = itemView.findViewById(R.id.userIdTextView)
        var userAvatar: ImageView = itemView.findViewById(R.id.avatarImageView)
    }

    override fun getItemCount(): Int {
        return users.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.user_recycler_item,parent,false)
        return UserViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        val user = users[position]
        holder.userName.text = user.login
        holder.userId.text = user.id.toString()

        val imageRequest = ImageRequest.Builder(context)
            .data(user.avatarUrl)
            .crossfade(true)
            .target(holder.userAvatar)
            .build()

        imageModule.imageLoader.enqueue(imageRequest)
        holder.itemView.setOnClickListener {
            userAdapterCallback(user)
        }
    }

}