package ru.leorikwhite.testgitusersapp.view

import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.activity.enableEdgeToEdge
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.lifecycle.Observer
import coil.request.ImageRequest
import dagger.hilt.android.AndroidEntryPoint
import ru.leorikwhite.testgitusersapp.R
import ru.leorikwhite.testgitusersapp.data.UserDetails
import ru.leorikwhite.testgitusersapp.di.ImageModule
import ru.leorikwhite.testgitusersapp.viewmodels.UserDetailsViewModel
import javax.inject.Inject

@AndroidEntryPoint
class UserDetailsActivity : AppCompatActivity() {

    @Inject lateinit var factory: UserDetailsViewModel.UserDetailsViewModelFactory

    private val viewModel:UserDetailsViewModel by viewModels() {
        UserDetailsViewModel.providesFactory(
            assistedFactory = factory,
            userName = intent.getStringExtra("userName") ?: ""
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_user_details)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }

        val imageView = findViewById<ImageView>(R.id.imageView)
        val userNameTextView = findViewById<TextView>(R.id.userNameTextView)
        val userEmailTextView = findViewById<TextView>(R.id.userEmailTextView)
        val userCompanyTextView = findViewById<TextView>(R.id.userCompanyTextView)
        val followingTextView = findViewById<TextView>(R.id.userFollowingTextView)
        val followersTextView = findViewById<TextView>(R.id.userFollowersTextView)
        val dateCreateTextView = findViewById<TextView>(R.id.userDateCreateTextView)

        val userDetailsObserver = Observer<UserDetails> {
            userDetails ->

            val imageRequest = ImageRequest.Builder(this.application)
                .data(userDetails.avatarUrl)
                .crossfade(true)
                .target(imageView)
                .build()
            ImageModule(this.application).imageLoader.enqueue(imageRequest)

            userNameTextView.text = userDetails.name
            userEmailTextView.text = userDetails.email
            userCompanyTextView.text = "Company: " + userDetails.company
            followingTextView.text = "Following count: " + userDetails.following
            followersTextView.text = "Followers count: " + userDetails.followers
            dateCreateTextView.text = "Date create: " + userDetails.createdAt
        }

        viewModel.userDetails.observe(this,userDetailsObserver)

    }
}