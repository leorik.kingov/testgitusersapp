package ru.leorikwhite.testgitusersapp.data

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers
import ru.leorikwhite.testgitusersapp.api.GitService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserRepository @Inject constructor(
   private val gitApi:GitService
) {

    fun getUsers(since:Int): Observable<List<User>> {
        return gitApi.getUsers(since)
    }

    fun getUserDetailsById(userName:String) : Observable<UserDetails> {
        return gitApi.getUserDetailsById(userName)
    }

}